from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import unittest

class NewVisitorTest(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.quit()

    def test_can_start_a_list_and_retrieve_it_later(self):
        # a user has heard about a new online to-do app, they go
        # to check out its homepage
        self.browser.get('http://localhost:8000')

        # they notice the page title and header mention to-do lists
        self.assertIn('To-Do', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('To-Do', header_text)

        # they are invited to enter a to-do item straight away
        inputbox = self.browser.find_element_by_id('id_new_item')

        self.assertEqual(
            inputbox.get_attribute('placeholder'), 
            'Enter a to-do item'
        )

        # they type "Do a django based tdd tutorial" into a text box 
        inputbox.send_keys('Do a django based tdd tutorial')

        # When they hit enter, the page updates, and now the page lists
        # "1: Do a django based tdd tutorial" as an item in a to-do list
        inputbox.send_keys(Keys.ENTER)
        time.sleep(1)

        table = self.browser.find_element_by_id('id_list_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertTrue(
            any(row.text == '1: Do a django based tdd tutorial' for row in rows),
            "New to-do item did not appear in table"
        )

        # There is still a text box inviting them to add another item. They
        # enter "Adapt tutorial to a piece of an existing code base"
        self.fail('Finish the test!')

        # The page updates again, and now shows both items on their list

        # They wonder whether the site will remember their list. Then they see
        # that the site has generated a unique URL for them -- there is some
        # explanatory text to that effect.

        # They visit that URL - their to-do list is still there.

        # Satisfied, they go back to sleep

if __name__ == '__main__':
    unittest.main(warnings='ignore')

